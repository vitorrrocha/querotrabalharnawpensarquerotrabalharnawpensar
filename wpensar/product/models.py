from django.db import models

class Product(models.Model):
    name = models.CharField(max_length=100)
    purchase = models.ForeignKey("Product" ,blank=True, null=True ,on_delete=models.CASCADE, related_name='purchases')

    class Meta:
        db_table = 'product'

    def __str__(self):
        return self.name

class Purchase(models.Model):
    qtd_product = models.IntegerField()
    total_value = models.FloatField(max_length=255)

    class Meta:
        db_table = 'purchase'


