from django.shortcuts import render, redirect

from .models import Product
from .form import ProductForm, PurchaseForm


def home(request):
    return render(request, 'product/home.html')

#product
def product_list(request):
    product ={}
    product['product'] = Product.objects.all()
    return render(request, 'product/list_products.html', product)

def create_product(request):
    new_product = {}
    form = ProductForm(request.POST or None)

    if form.is_valid():
        form.save()
        return redirect('listar-produtos')

    new_product['form'] = form
    return render(request, 'product/product_form.html', new_product)

#purchase
def make_purchase(request):
    product = {}
    new_purchase = {}

    product['product'] = Product.objects.all()

    form = PurchaseForm(request.POST or None)

    if form.is_valid():
        form.save()
        return redirect('listar-produtos')

    new_purchase['form'] = form

    return render(request, 'purchase/purchase_form.html', new_purchase)

# def product_list(request):
#     product ={}
#     product['product'] = Product.objects.all()
#     return render(request, 'product/list_products.html', product)
