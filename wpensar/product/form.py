from django.forms import ModelForm
from django import forms
from .models import Product, Purchase

class ProductForm(ModelForm):
    class Meta:
        model = Product
        fields = ['name']

class PurchaseForm(forms.Form):
    class Meta:
        model = Purchase, Product
        fields = ['name']


